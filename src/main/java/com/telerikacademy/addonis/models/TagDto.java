package com.telerikacademy.addonis.models;

/**
 * Tag model DTO used when tags are added to addon.
 */

public class TagDto {
    private String tagName;

    public TagDto() {
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }
}
