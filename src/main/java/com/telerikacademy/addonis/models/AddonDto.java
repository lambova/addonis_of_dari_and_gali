package com.telerikacademy.addonis.models;

import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

/**
 * Addon model DTO used when addon is created or updated.
 */

public class AddonDto {

    private static final String ADDON_NAME_NOT_VALID = "Addon-name cannot be less than 1 and more than 50 symbols.";
    private static final String ADDON_NO_NAME = "Addon-name is mandatory.";
    private static final String NO_LINK = "Link to the addon's repo in GitHub is mandatory.";

    private String id;

    private boolean isExisting;

    private boolean hasRightsToModify;

    @Size (min = 1, max = 50, message = ADDON_NAME_NOT_VALID)
    @NotEmpty(message = ADDON_NO_NAME)
    private String name;

    private String description;

    @URL
    @NotEmpty(message = NO_LINK)
    private String linkOrigin;

    private Byte[] image;

    private Byte[] plugin;

    public AddonDto() {
    }

    public boolean isExisting() {
        return isExisting;
    }

    public boolean hasRightsToModify() {
        return hasRightsToModify;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getLinkOrigin() {
        return linkOrigin;
    }

    public Byte[] getImage() {
        return image;
    }

    public Byte[] getPlugin() {
        return plugin;
    }


    public void setExisting(boolean existing) {
        isExisting = existing;
    }

    public void setHasRightsToModify(boolean hasRightsToModify) {
        this.hasRightsToModify = hasRightsToModify;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setLinkOrigin(String linkOrigin) {
        this.linkOrigin = linkOrigin;
    }

    public void setImage(Byte[] image) {
        this.image = image;
    }

    public void setPlugin(Byte[] plugin) {
        this.plugin = plugin;
    }
}
