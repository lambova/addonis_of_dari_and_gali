package com.telerikacademy.addonis.models;

/**
 * Rating model DTO used when addon is rated.
 */

public class RatingDTO {

    int rating;
    Double averageRating;

    public RatingDTO() {
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Double getAverageRating() {
        return averageRating;
    }

    public void setAverageRating(Double averageRating) {
        this.averageRating = averageRating;
    }
}
