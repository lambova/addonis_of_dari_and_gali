package com.telerikacademy.addonis.models;

/**
 * Password model DTO used when password is changed.
 */

public class PasswordDTO {

    private String token;

    private String newPassword;

    public PasswordDTO() {
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
