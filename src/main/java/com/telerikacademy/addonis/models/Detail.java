package com.telerikacademy.addonis.models;

import javax.persistence.*;

/**
 * Detail model - POJO class for the data taken from GitHub.
 */

@Entity
@Table(name = "details")
public class Detail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "details_id")
    private long id;

    @Column(name = "count_open_issues")
    private int countOpenIssues;

    @Column(name = "count_pull_requests")
    private int countPullRequests;

    @Column(name = "date_last_commit")
    private String dateOfLastCommit;

    @Column(name = "message_last_commit")
    private String messageOfLastCommit;

    @OneToOne(mappedBy = "detail")
    private Addon addon;

    public Detail() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCountOpenIssues() {
        return countOpenIssues;
    }

    public void setCountOpenIssues(int countOpenIssues) {
        this.countOpenIssues = countOpenIssues;
    }

    public int getCountPullRequests() {
        return countPullRequests;
    }

    public void setCountPullRequests(int countPullRequests) {
        this.countPullRequests = countPullRequests;
    }

    public String getDateOfLastCommit() {
        return dateOfLastCommit;
    }

    public void setDateOfLastCommit(String dateOfLastCommit) {
        this.dateOfLastCommit = dateOfLastCommit;
    }

    public String getMessageOfLastCommit() {
        return messageOfLastCommit;
    }

    public void setMessageOfLastCommit(String messageOfLastCommit) {
        this.messageOfLastCommit = messageOfLastCommit;
    }

    public Addon getAddon() {
        return addon;
    }

    public void setAddon(Addon addon) {
        this.addon = addon;
    }
}
