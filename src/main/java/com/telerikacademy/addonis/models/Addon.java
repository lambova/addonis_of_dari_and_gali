package com.telerikacademy.addonis.models;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.Date;

/**
 * Addon model - POJO class.
 */

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "addons")
public class Addon {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "addon_id")
    private String id;

    @Column(name = "addon_name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "number_of_downloads")
    private int numberOfDownloads;

    @Column(name = "link_origin")
    private String linkOrigin;

    @ManyToOne
    @JoinColumn(name = "creator_username")
    private User creator;

    @Column(name = "image")
    private Byte[] image;

    @Column(name = "binary_content")
    private Byte[] plugin;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "details_id")
    private Detail detail;

    @Column(name = "is_enabled")
    private boolean isEnabled;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "addons_tags",
            joinColumns = @JoinColumn(name = "addon_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private Set<Tag> tags;

    @CreatedDate
    @Column(name = "date_of_creation")
    private Date dateOfCreation;

    public Addon() {
    }

    public Addon (String id, String name, String description, String linkOrigin){
        this.id = id;
        this.name = name;
        this.description = description;
        this.linkOrigin = linkOrigin;
        tags = new HashSet<>();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getNumberOfDownloads() {
        return numberOfDownloads;
    }

    public String getLinkOrigin() {
        return linkOrigin;
    }

    public User getCreator() {
        return creator;
    }

    public Byte[] getImage() {
        return image;
    }

    public Byte[] getPlugin() {
        return plugin;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setNumberOfDownloads(int numberOfDownloads) {
        this.numberOfDownloads = numberOfDownloads;
    }

    public void setLinkOrigin(String linkOrigin) {
        this.linkOrigin = linkOrigin;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public void setImage(Byte[] image) {
        this.image = image;
    }

    public void setPlugin(Byte[] plugin) {
        this.plugin = plugin;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public Detail getDetail() {
        return detail;
    }

    public void setDetail(Detail detail) {
        this.detail = detail;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public void addTag(Tag tag) {
        tags.add(tag);
    }

    public void removeTag(Tag tag) {
        tags.remove(tag);
    }

    public Date isDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }
}
