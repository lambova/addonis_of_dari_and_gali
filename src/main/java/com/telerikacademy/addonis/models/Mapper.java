package com.telerikacademy.addonis.models;

import com.telerikacademy.addonis.services.AddonOperationsHelper;
import com.telerikacademy.addonis.services.AddonsService;
import com.telerikacademy.addonis.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * Mapper - helper class for mapping dto to its model and vice versa.
 */

@Component
public class Mapper {
    private AddonsService addonsService;
    private UsersService usersService;

    @Autowired
    public Mapper(AddonsService addonsService, UsersService usersService) {
        this.addonsService = addonsService;
        this.usersService = usersService;
    }

    public Addon mapDtoToAddon(AddonDto addonDto, Addon addon) {
        if (addonDto.getName() != null) {
            addon.setName(addonDto.getName());
        }
        if (addonDto.getDescription() != null) {
            addon.setDescription(addonDto.getDescription());
        }
        if (addonDto.getLinkOrigin() != null) {
            addon.setLinkOrigin(addonDto.getLinkOrigin());
        }
        return addon;
    }

    public Addon mapFilesDtoToAddon(Addon addon, MultipartFile imageFile, MultipartFile pluginFile) {
        if (!imageFile.isEmpty()) {
            addon.setImage(AddonOperationsHelper.getBytes(imageFile));
        }
        if (!pluginFile.isEmpty()) {
            addon.setPlugin(AddonOperationsHelper.getBytes(pluginFile));
        }
        return addon;
    }

    public AddonDto mapDtoToShow(AddonDto addonDto, Addon addon) {
        addonDto.setId(addon.getId());
        addonDto.setName(addon.getName());
        addonDto.setDescription(addon.getDescription());
        addonDto.setLinkOrigin(addon.getLinkOrigin());
        return addonDto;
    }

    public Rating mapRatingDtoToRating(String addonId, String name, RatingDTO ratingDTO) {
        User user = usersService.getUserByName(name);
        Addon addon = addonsService.findById(addonId);
        Rating rating = new Rating();
        rating.setRating(ratingDTO.getRating());
        rating.setUser(user);
        rating.setAddon(addon);

        return rating;
    }

    public void mapDtoToTag(TagDto tagDto, Tag newTag) {
        newTag.setTagName(tagDto.getTagName());
    }

}
