package com.telerikacademy.addonis.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Objects;
import java.util.Set;

/**
 * Tag model - POJO class.
 */

@Entity
@Table(name = "tags", schema = "addonis")
public class Tag {

    private static final String TAG_NAME_NOT_VALID = "Tag-name cannot be less than 1 and more than 30 symbols.";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "tag_id")
    private long tagId;

    @Column(name = "tag_name")
    @Size(min = 1, max = 30, message = TAG_NAME_NOT_VALID)
    private String tagName;

    @ManyToMany(mappedBy = "tags", fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<Addon> addons;

    public Tag() {
    }

    public long getTagId() {
        return tagId;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagId(long tagId) {
        this.tagId = tagId;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public Set<Addon> getAddons() {
        return addons;
    }

    public void setAddons(Set<Addon> addons) {
        this.addons = addons;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tag tag = (Tag) o;
        return Objects.equals(tagName, tag.tagName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tagName);
    }
}
