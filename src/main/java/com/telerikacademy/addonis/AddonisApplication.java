package com.telerikacademy.addonis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Starting point of the application.
 */

@SpringBootApplication
public class AddonisApplication {

    public static void main(String[] args) {
        SpringApplication.run(AddonisApplication.class, args);
    }

}
