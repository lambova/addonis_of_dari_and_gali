package com.telerikacademy.addonis.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Custom exception when the authorized user has no rights to fulfill the required operation.
 */


@ResponseStatus(HttpStatus.FORBIDDEN)
public class InvalidOperationException extends RuntimeException {

    public InvalidOperationException (String type, String attribute, String operation) {
        super(String.format("%s with username %s has no rights to %s.", type, attribute, operation));
    }
}
