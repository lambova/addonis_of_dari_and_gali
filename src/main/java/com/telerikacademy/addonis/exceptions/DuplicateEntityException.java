package com.telerikacademy.addonis.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Custom exception for duplicate entries.
 */

@ResponseStatus(HttpStatus.CONFLICT)
public class DuplicateEntityException extends RuntimeException {

        public DuplicateEntityException(String type, String attribute) {
        super(String.format("%s with %s already exists.", type, attribute));
    }

    public DuplicateEntityException(String type, String attribute, String value) {
        super(String.format("%s with %s '%s' already exists.", type, attribute, value));
    }
}