package com.telerikacademy.addonis.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Custom exception when searching for non-existing entry.
 */

@ResponseStatus(HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends RuntimeException {

    public EntityNotFoundException(String type, String attribute) {
        super(String.format("%s with %s does not exist.",type, attribute));
    }
    public EntityNotFoundException(String type, long attribute) {
        super(String.format("%s with %s does not exist.",type, attribute));
    }
}