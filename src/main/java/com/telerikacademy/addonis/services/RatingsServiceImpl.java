package com.telerikacademy.addonis.services;

import com.telerikacademy.addonis.models.Rating;

import com.telerikacademy.addonis.repositories.RatingsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service layer - implementation class for Ratings.
 */


@Service
public class RatingsServiceImpl implements RatingsService {
    private RatingsRepository ratingsRepository;

    @Autowired
    public RatingsServiceImpl(RatingsRepository ratingsRepository) {
        this.ratingsRepository = ratingsRepository;
    }

    @Override
    public void addRatingToAddon(Rating rating) {
        if (ratingsRepository.existsByUser_UsernameAndAddon_Id(
                rating.getUser().getUsername(), rating.getAddon().getId())) {

            Rating oldRating = ratingsRepository.
                    findByUser_UsernameAndAddon_Id(rating.getUser().getUsername(), rating.getAddon().getId());
            oldRating.setRating(rating.getRating());

            ratingsRepository.save(oldRating);

        } else {
            ratingsRepository.save(rating);
        }
    }

    @Override
    public Double getAverageAddonRating(String addonId) {
        return ratingsRepository.findAverageAddonRating(addonId);
    }
}
