package com.telerikacademy.addonis.services;

import com.telerikacademy.addonis.models.Addon;
import com.telerikacademy.addonis.models.Tag;
import com.telerikacademy.addonis.models.User;

import java.security.Principal;
import java.util.List;
import java.util.Set;

/**
 * Service layer - interface for Addons.
 */

public interface AddonsService {

    List<Addon> getAllAddons();

    List<Addon> getAllPendingAddons();

    List<Addon> getAllUserAddons(String username);

    Addon findById(String id);

    Addon findByName(String name);

    Addon createAddon(Addon addon, Principal principal);

    Addon update(Addon addon, User currentUser, boolean isAdmin);

    Addon update(Addon addon);

    Addon delete(String id, User currentUser, boolean isAdmin);

    Addon addTag(String addonId, Tag tag);

    Addon removeTag(String addonId, String tagName);

    Set<Addon> filterByTag(long tagId);

    List<Tag> getAllTags();

    void enableAddon(String addonId);

    List<Addon> sortByName();

    List<Addon> sortByNameDesc();

    List<Addon> sortByNumberOfDownloads();

    List<Addon> sortByDateOfCreation();

    List<Addon> sortByDateOfLastCommit();

    List<Addon> sort(String sortType);
}
