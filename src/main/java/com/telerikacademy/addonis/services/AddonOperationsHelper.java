package com.telerikacademy.addonis.services;

import com.telerikacademy.addonis.exceptions.InvalidDataException;
import com.telerikacademy.addonis.models.Addon;
import com.telerikacademy.addonis.models.User;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;

/**
 * Helper class for operations on Addons.
 */

public class AddonOperationsHelper {

    public static boolean canUserModifyAddon(Addon addon, User currentUser, boolean isAdmin) {
        boolean hasRightsToModify = currentUser.getUsername().equals(addon.getCreator().getUsername());
        if (isAdmin) {
            hasRightsToModify = true;
        }
        return hasRightsToModify;
    }

    public static boolean userHasRightsToModify(HttpServletRequest request, Addon addon, UsersService usersService) {
        Principal principal = request.getUserPrincipal();
        User currentUser = usersService.getUserByName(principal.getName());
        boolean isAdmin = request.isUserInRole("ROLE_ADMIN");
        return AddonOperationsHelper.canUserModifyAddon(addon, currentUser, isAdmin);
    }

    public static Byte[] getBytes(MultipartFile file) {
        try {
            Byte[] imageFile = new Byte[file.getBytes().length];
            int i = 0;
            for (byte b : file.getBytes()) {
                imageFile[i++] = b;
            }
            return imageFile;
        } catch (IOException e) {
            throw new InvalidDataException("file");
        }
    }

    public static void getByteArray(Byte[] byteArray, HttpServletResponse response) {
        try {
            if (byteArray != null) {
                byte[] byteA = new byte[byteArray.length];
                int i = 0;
                for (Byte b : byteArray) {
                    byteA[i++] = b;
                }
                response.setContentType("image/jpeg/png/application");
                InputStream stream = new ByteArrayInputStream(byteA);
                IOUtils.copy(stream, response.getOutputStream());
            }
        } catch (IOException e) {
            throw new InvalidDataException("download");
        }
    }

}
