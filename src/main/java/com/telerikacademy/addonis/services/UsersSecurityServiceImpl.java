package com.telerikacademy.addonis.services;

import com.telerikacademy.addonis.models.VerificationToken;
import com.telerikacademy.addonis.repositories.VerificationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Calendar;

/**
 * Service layer - implementation class for password verification tokens.
 */

@Service
@Transactional
public class UsersSecurityServiceImpl implements UsersSecurityService {
    private VerificationTokenRepository verificationTokenRepository;

    @Autowired
    public UsersSecurityServiceImpl(VerificationTokenRepository verificationTokenRepository) {
        this.verificationTokenRepository = verificationTokenRepository;
    }

    @Override
    public String validatePasswordResetToken(String token) {
        VerificationToken passToken = verificationTokenRepository.findByToken(token);

        return !isTokenFound(passToken) ? "invalid"
                : isTokenExpired(passToken) ? "expired"
                : null;
    }

    private boolean isTokenFound(VerificationToken passToken) {
        return passToken != null;
    }

    private boolean isTokenExpired(VerificationToken passToken) {
        final Calendar cal = Calendar.getInstance();
        return passToken.getExpiryDate().before(cal.getTime());
    }
}
