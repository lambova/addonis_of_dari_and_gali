package com.telerikacademy.addonis.services;

import com.telerikacademy.addonis.models.User;
import com.telerikacademy.addonis.models.VerificationToken;

import java.util.Collection;
import java.util.Optional;

/**
 * Service layer - interface for Users.
 */

public interface UsersService {
    Collection<User> getAll();
    User getUserByName(String username);
    User createUser(User user);
    void disableUser(String username);
    void enableUser(String username);
    void createVerificationToken(User user, String token);
    VerificationToken getVerificationToken(String token);
    User findUserByEmail(String email);
    User getUserByPasswordResetToken(String token);
    void changeUserPassword(User user, String newPassword);
}
