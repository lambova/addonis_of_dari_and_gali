package com.telerikacademy.addonis.services;

/**
 * Service layer - interface for password validation tokens.
 */

public interface UsersSecurityService {
    String validatePasswordResetToken(String token);
}
