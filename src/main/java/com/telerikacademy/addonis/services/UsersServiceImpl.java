package com.telerikacademy.addonis.services;

import com.telerikacademy.addonis.exceptions.*;
import com.telerikacademy.addonis.models.User;
import com.telerikacademy.addonis.models.VerificationToken;
import com.telerikacademy.addonis.repositories.UsersRepository;
import com.telerikacademy.addonis.repositories.VerificationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Service layer - implementation class for Users.
 */


@Service
public class UsersServiceImpl implements UsersService {

    private UsersRepository usersRepository;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private VerificationTokenRepository tokenRepository;

    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository, UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder, VerificationTokenRepository tokenRepository) {
        this.usersRepository = usersRepository;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.tokenRepository = tokenRepository;
    }

    @Override
    public Collection<User> getAll() {
        return usersRepository.findAll();
    }

    @Override
    public User getUserByName(String username) {
        if (!userDetailsManager.userExists(username)) {
            throw new EntityNotFoundException("User", username);
        }
        return usersRepository.findByUsername(username);
    }

    @Override
    public User createUser(User user) {
        if (userDetailsManager.userExists(user.getUsername())) {
            throw new DuplicateEntityException("User", user.getUsername());
        }

        user.setPassword(passwordEncoder.encode(user.getPassword()));
      //  user.setEnabled(true);
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        user.getUsername(),
                        user.getPassword(),
                        authorities);
        userDetailsManager.createUser(newUser);
        return usersRepository.save(user);
    }

    @Override
    public void disableUser(String username) {
        User user = getUserByName(username);
        user.setEnabled(false);
        usersRepository.save(user);
    }

    @Override
    public void enableUser(String username) {
        User user = getUserByName(username);
        user.setEnabled(true);
        usersRepository.save(user);
    }

    @Override
    public void createVerificationToken(User user, String token) {
        VerificationToken myToken = new VerificationToken(token, user);
        tokenRepository.save(myToken);
    }

    @Override
    public VerificationToken getVerificationToken(String token) {
        return tokenRepository.findByToken(token);
    }

    @Override
    public User findUserByEmail(String email) {
        if (usersRepository.findUserByEmail(email) == null) {
            throw new EntityNotFoundException("Email", email);
        }
        return usersRepository.findUserByEmail(email);
    }

    @Override
    public User getUserByPasswordResetToken(String token) {
        return tokenRepository.findByToken(token).getUser();
    }

    @Override
    public void changeUserPassword(User user, String newPassword) {
        user.setPassword(passwordEncoder.encode(newPassword));
        usersRepository.save(user);
    }
}
