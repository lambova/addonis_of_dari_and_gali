package com.telerikacademy.addonis.services;

import com.telerikacademy.addonis.exceptions.DuplicateEntityException;
import com.telerikacademy.addonis.exceptions.EntityNotFoundException;
import com.telerikacademy.addonis.exceptions.InvalidOperationException;
import com.telerikacademy.addonis.models.Addon;
import com.telerikacademy.addonis.models.Tag;
import com.telerikacademy.addonis.models.User;
import com.telerikacademy.addonis.repositories.AddonsRepository;
import com.telerikacademy.addonis.repositories.GitHubDetailsRepository;
import com.telerikacademy.addonis.repositories.TagsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import java.security.Principal;
import java.util.List;
import java.util.Set;

/**
 * Service layer - implementation class for Addons.
 */

@Service
public class AddonsServiceImpl implements AddonsService {

    private AddonsRepository addonsRepository;
    private GitHubDetailsRepository gitHubDetailsRepository;
    private UsersService usersService;
    private TagsRepository tagsRepository;

    @Autowired
    public AddonsServiceImpl(AddonsRepository addonsRepository,
                             GitHubDetailsRepository gitHubDetailsRepository,
                             TagsRepository tagsRepository,
                             UsersService usersService) {
        this.addonsRepository = addonsRepository;
        this.gitHubDetailsRepository = gitHubDetailsRepository;
        this.tagsRepository = tagsRepository;
        this.usersService = usersService;
    }

    @Override
    public List<Addon> getAllAddons() {
        return addonsRepository.getAllAddons();
    }

    @Override
    public List<Addon> getAllPendingAddons() {
       return addonsRepository.getAllPendingAddons();
    }

    @Override
    public List<Addon> getAllUserAddons(String username) {
        return addonsRepository.getAllUserAddons(username);
    }

    @Override
    public Addon findByName(String name) {
        if (!checkAddonNameExists(name)) {
            throw new EntityNotFoundException("Addon", name);
        }
        return addonsRepository.findByName(name);
    }

    @Override
    public Addon findById(String id) {
        return addonsRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Id", id));
    }

    @Override
    public Addon createAddon(Addon addon, Principal principal) {
        String addonName = addon.getName();
        if (checkAddonNameExists(addonName)) {
            throw new DuplicateEntityException("Addon", "name", addonName);
        }
        addon.setDetail(gitHubDetailsRepository.createAddonDetails(addon.getLinkOrigin()));
        addon.setCreator(usersService.getUserByName(principal.getName()));
        return addonsRepository.save(addon);
    }

    @Transactional
    @Override
    public Addon update(Addon addon, User currentUser, boolean isAdmin) {
        if (!AddonOperationsHelper.canUserModifyAddon(addon, currentUser, isAdmin)) {
            throw new InvalidOperationException("User", currentUser.getUsername(), "modify");
        }
        addon.setDetail(gitHubDetailsRepository
                .updateAddonDetails(addon.getLinkOrigin(),
                        addon.getDetail().getId()));
        addonsRepository.save(addon);
        return addon;
    }

    @Transactional
    @Override
    public Addon update(Addon addon) {
        addonsRepository.save(addon);
        return addon;
    }


    @Override
    public Addon delete(String id, User currentUser, boolean isAdmin) {
        Addon addon = findById(id);
        if (!AddonOperationsHelper.canUserModifyAddon(addon, currentUser, isAdmin)) {
            throw new InvalidOperationException("User", currentUser.getUsername(), "delete");
        }
        addon.setDeleted(true);
        addonsRepository.save(addon);

        return addon;
    }


    @Override
    public Addon addTag(String addonId, Tag tag) {
        if (!tagsRepository.findAll().contains(tag)) {
            tagsRepository.save(tag);
        }
        tag = tagsRepository.findByTagName(tag.getTagName());
        Addon addon = findById(addonId);
        addon.addTag(tag);
        addonsRepository.save(addon);
        return addon;
    }

    @Override
    public Addon removeTag(String addonId, String tagName) {
        Addon addon = findById(addonId);
        Tag tag = tagsRepository.findByTagName(tagName);
        addon.removeTag(tag);
        addonsRepository.save(addon);
        return addon;
    }

    @Override
    public Set<Addon> filterByTag(long tagId) {
        return tagsRepository.filterByTag(tagId);
    }

    @Override
    public List<Tag> getAllTags() {
        return tagsRepository.findAll();
    }

    @Override
    public void enableAddon(String addonId) {
        Addon addon = findById(addonId);
        addon.setEnabled(true);
        addonsRepository.save(addon);
    }

    @Override
    public List<Addon> sortByName() {
        return addonsRepository.findAllOrderByName();
    }

    @Override
    public List<Addon> sortByNameDesc() {
        return addonsRepository.findAllOrderByNameDesc();
    }

    @Override
    public List<Addon> sortByNumberOfDownloads() {
        return addonsRepository.findAllOrderByNumberOfDownloads();
    }

    @Override
    public List<Addon> sortByDateOfCreation() {
        return addonsRepository.findAllOrderByDateOfCreation();
    }

    @Override
    public List<Addon> sortByDateOfLastCommit() {
        return addonsRepository.findAllOrderByDetailDateOfLastCommit();
    }

    @Override
    public List<Addon> sort(String sortType) {
        switch (sortType) {
            case "name":
                return sortByName();
            case "name_desc":
                return sortByNameDesc();
            case "downloads":
                return sortByNumberOfDownloads();
            case "upload_date":
                return sortByDateOfCreation();
            case "commit_date":
                return sortByDateOfLastCommit();
        }
        return getAllAddons();
    }


    private boolean checkAddonNameExists(String username) {
        return addonsRepository.findByName(username) != null;
    }

}

