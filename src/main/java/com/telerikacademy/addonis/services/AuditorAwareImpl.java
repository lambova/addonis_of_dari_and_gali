package com.telerikacademy.addonis.services;

import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

/**
 * Implementation of auditor for getting date of creation of addon.
 */

public class AuditorAwareImpl implements AuditorAware<String> {
    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.of("Current Auditor");
    }
}

