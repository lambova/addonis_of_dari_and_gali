package com.telerikacademy.addonis.services;

import com.telerikacademy.addonis.models.Rating;

/**
 * Service layer - interface for Ratings.
 */

public interface RatingsService {
    void addRatingToAddon(Rating rating);
    Double getAverageAddonRating(String addonName);
}
