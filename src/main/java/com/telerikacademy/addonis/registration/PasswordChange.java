package com.telerikacademy.addonis.registration;

import com.telerikacademy.addonis.models.User;
import com.telerikacademy.addonis.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class PasswordChange implements ApplicationListener<OnPasswordChangeCompleteEvent>{

    private UsersService usersService;
    private MessageSource messageSource;
    private JavaMailSender javaMailSender;

    @Autowired
    public PasswordChange(UsersService usersService, MessageSource messageSource, JavaMailSender javaMailSender) {
        this.usersService = usersService;
        this.messageSource = messageSource;
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void onApplicationEvent(OnPasswordChangeCompleteEvent event)  {
        this.confirmPasswordChange(event);
    }

    private void confirmPasswordChange(OnPasswordChangeCompleteEvent event) {
        User user = event.getUser();
        String token = UUID.randomUUID().toString();
        usersService.createVerificationToken(user, token);

        SimpleMailMessage email = constructEmailMessage(event, user, token);
        javaMailSender.send(email);
    }

    private SimpleMailMessage constructEmailMessage(OnPasswordChangeCompleteEvent event, User user, Object token) {
        String recipientAddress = user.getEmail();
        String subject = "Addonis Password Reset";
        String confirmationUrl
                = event.getAppUrl() + "/change-password?token=" + token;

        String message = messageSource.getMessage("message.resetPassword", null, event.getLocale());

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText(message + "\r\n" + "http://localhost:8080" + confirmationUrl);

        return email;
    }



}