package com.telerikacademy.addonis.registration;

import com.telerikacademy.addonis.models.User;
import com.telerikacademy.addonis.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Implementation of a registration listener that handles user confirmation email.
 */

@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {

    private UsersService usersService;
    private MessageSource messageSource;
    private JavaMailSender javaMailSender;

    @Autowired
    public RegistrationListener(UsersService usersService, MessageSource messageSource, JavaMailSender javaMailSender) {
        this.usersService = usersService;
        this.messageSource = messageSource;
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void onApplicationEvent(OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }

    private void confirmRegistration(OnRegistrationCompleteEvent event) {
        User user = event.getUser();
        String token = UUID.randomUUID().toString();
        usersService.createVerificationToken(user, token);

        SimpleMailMessage email = constructEmailMessage(event, user, token);
        javaMailSender.send(email);
    }

    private SimpleMailMessage constructEmailMessage(OnRegistrationCompleteEvent event, User user, Object token) {
        String recipientAddress = user.getEmail();
        String subject = "Addonis Registration Confirmation";
        String confirmationUrl
                = event.getAppUrl() + "/confirm-registration?token=" + token;

        String message = messageSource.getMessage("message.regSucc", null, event.getLocale());

        SimpleMailMessage email = new SimpleMailMessage();
        email.setTo(recipientAddress);
        email.setSubject(subject);
        email.setText(message + "\r\n" + "http://localhost:8080" + confirmationUrl);

        return email;
    }
}