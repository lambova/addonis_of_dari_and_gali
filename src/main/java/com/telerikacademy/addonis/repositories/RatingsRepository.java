package com.telerikacademy.addonis.repositories;

import com.telerikacademy.addonis.models.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * JPA repository for Ratings.
 */

public interface RatingsRepository extends JpaRepository<Rating, Long> {

    boolean existsByUser_UsernameAndAddon_Id(String username,  String addonId);

    Rating findByUser_UsernameAndAddon_Id(String username, String addonId);

    @Query("select avg(r.rating) from Rating r where r.addon.id =:addonId")
    Double findAverageAddonRating(String addonId);

}

