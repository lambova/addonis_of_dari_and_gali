package com.telerikacademy.addonis.repositories;

import com.telerikacademy.addonis.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * JPA repository for Users.
 */

public interface UsersRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
    User findUserByEmail(String email);
}
