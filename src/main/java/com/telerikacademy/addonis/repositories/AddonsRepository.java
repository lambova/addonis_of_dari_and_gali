package com.telerikacademy.addonis.repositories;

import com.telerikacademy.addonis.models.Addon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

/**
 * JPA repository for Addons.
 */

public interface AddonsRepository extends JpaRepository<Addon, String> {

    @Query("FROM Addon a WHERE a.isDeleted = false AND a.isEnabled = true")
    List<Addon> getAllAddons();

    @Query("FROM Addon a WHERE a.isDeleted = false AND a.isEnabled = false")
    List<Addon> getAllPendingAddons();

    @Query("FROM Addon a WHERE a.isDeleted = false AND a.creator.username =:username")
    List<Addon> getAllUserAddons(String username);

    Addon findByName(String name);


    @Query("FROM Addon a WHERE a.isDeleted= false AND a.isEnabled = true ORDER BY a.name DESC")
    List <Addon> findAllOrderByNameDesc();

    @Query("FROM Addon a WHERE a.isDeleted= false AND a.isEnabled = true ORDER BY a.name")
    List <Addon> findAllOrderByName();

    @Query("FROM Addon a WHERE a.isDeleted= false AND a.isEnabled = true ORDER BY a.numberOfDownloads DESC")
    List <Addon> findAllOrderByNumberOfDownloads();

    @Query("FROM Addon a WHERE a.isDeleted= false AND a.isEnabled = true ORDER BY a.dateOfCreation DESC")
    List <Addon> findAllOrderByDateOfCreation();


    @Query("FROM Addon a JOIN Detail d ON a.detail.id = d.id " +
            "WHERE a.isDeleted= false AND a.isEnabled = true " +
            "group by d.dateOfLastCommit")
    List <Addon> findAllOrderByDetailDateOfLastCommit();

    List <Addon> findTop3ByDateOfCreation(Date date);

    List <Addon> findTopByDateOfCreation(Date date);

    boolean existsById (String id);

}
