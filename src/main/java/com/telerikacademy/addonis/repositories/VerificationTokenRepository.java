package com.telerikacademy.addonis.repositories;

import com.telerikacademy.addonis.models.User;
import com.telerikacademy.addonis.models.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * JPA repository for verification tokens.
 */

public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Long> {
    VerificationToken findByToken(String token);

}