package com.telerikacademy.addonis.repositories;

import com.telerikacademy.addonis.models.Addon;
import com.telerikacademy.addonis.models.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Set;

/**
 * JPA repository for Tags.
 */

public interface TagsRepository extends JpaRepository<Tag, Long> {

    Tag findByTagName(String tagName);

    @Query("select distinct a from Addon a join a.tags at where at.tagId = :id")
    Set<Addon> filterByTag(@Param("id") long tagId);
}
