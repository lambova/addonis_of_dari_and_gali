package com.telerikacademy.addonis.repositories;

import com.telerikacademy.addonis.models.Detail;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * JPA repository for Details.
 */

public interface DetailsRepository extends JpaRepository<Detail, Long> {
}
