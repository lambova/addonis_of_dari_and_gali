package com.telerikacademy.addonis.repositories;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.telerikacademy.addonis.exceptions.EntityNotFoundException;
import com.telerikacademy.addonis.exceptions.InvalidDataException;
import com.telerikacademy.addonis.models.Detail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Repository for data from GitHub.
 */

@Component
public class GitHubDetailsRepository {
    private DetailsRepository detailsRepository;

    @Autowired
    public GitHubDetailsRepository(DetailsRepository detailsRepository) {
        this.detailsRepository = detailsRepository;
    }

    public Detail createAddonDetails(String originLink) {
        String repoLink = getOriginLink(originLink);
        Detail detail = new Detail();

        return getDetail(repoLink, detail);
    }

    Detail getDetail(String repoLink, Detail detail) {
        try {
            URL urlForGetRequest = new URL(repoLink);
            HttpURLConnection connection = getHttpURLConnection(urlForGetRequest);

            getIssuesCount(detail, connection);
            getPullRequests(repoLink, detail);
            getCommits(repoLink, detail);

        } catch (IOException e) {
            throw new InvalidDataException("github repo");
        }

        return detail;
    }

    public Detail updateAddonDetails(String originLink, long id) {
        String repoLink = getOriginLink(originLink);
        Detail detail = detailsRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Id", id));

        return getDetail(repoLink, detail);
    }

    private String getOriginLink(String originLink) {
        String[] gitHubLink = originLink.split("/");
        String ownerName = gitHubLink[(gitHubLink.length - 2)];
        String repoName = gitHubLink[(gitHubLink.length - 1)];
        String baseUrl = "https://api.github.com/repos/";
        return String.format("%s%s/%s", baseUrl, ownerName, repoName);
    }

    private void getCommits(Object repoLink, Detail detail) throws IOException {
        URL urlForGetRequestCommits = new URL(repoLink + "/commits");
        HttpURLConnection connectionCommits = getHttpURLConnection(urlForGetRequestCommits);
        int responseCodeCommits = connectionCommits.getResponseCode();
        if (responseCodeCommits == HttpURLConnection.HTTP_OK) {
            String json = getJson(connectionCommits);
            JsonArray convertedObject = new Gson().fromJson(json, JsonArray.class);
            String commit = String.valueOf(((JsonObject) convertedObject.get(0)).get("commit"));
            JsonObject jsonObject = new JsonParser().parse(commit).getAsJsonObject();
            String date = jsonObject.getAsJsonObject("author").get("date").getAsString();
            String message = jsonObject.getAsJsonPrimitive("message").toString();
            detail.setDateOfLastCommit(date);
            detail.setMessageOfLastCommit(message);
//            detailsRepository.save(detail);

        }
    }

    private void getPullRequests(Object repoLink, Detail detail) throws IOException {
        URL urlForGetRequestPulls = new URL(repoLink + "/pulls");
        HttpURLConnection connectionPulls = getHttpURLConnection(urlForGetRequestPulls);
        int responseCodePulls = connectionPulls.getResponseCode();
        if (responseCodePulls == HttpURLConnection.HTTP_OK) {
            String json = getJson(connectionPulls);
            JsonArray convertedObject = new Gson().fromJson(json, JsonArray.class);
            detail.setCountPullRequests(convertedObject.size());
        }
    }

    private void getIssuesCount(Detail detail, HttpURLConnection connection) throws IOException {
        int responseCode = connection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            String json = getJson(connection);
            JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
            String issues = jsonObject.getAsJsonPrimitive("open_issues_count").toString();
            detail.setCountOpenIssues(Integer.parseInt(issues));
        }
    }

    private HttpURLConnection getHttpURLConnection(URL urlForGetRequest) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) urlForGetRequest.openConnection();
        connection.setRequestMethod("GET");

        return connection;
    }

    private String getJson(HttpURLConnection connection) throws IOException {
        String readLine;
        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        StringBuilder response = new StringBuilder();
        while ((readLine = in.readLine()) != null) {
            response.append(readLine);
        }
        in.close();
        return response.toString();
    }

}
