package com.telerikacademy.addonis.controllers;

import com.telerikacademy.addonis.models.Tag;
import com.telerikacademy.addonis.services.AddonsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;

/**
 * MVC controller for landing page.
 */

@Controller
public class HomeController {
    private AddonsService addonsService;

    @Autowired
    public HomeController(AddonsService addonsService) {
        this.addonsService = addonsService;
    }

    @ModelAttribute("tags")
    public List<Tag> populateTags() {
        return addonsService.getAllTags();
    }

    @GetMapping("/")
    public String showHomePage(Model model) {
        model.addAttribute("addons", addonsService.getAllAddons());
        model.addAttribute("sortedByDate", addonsService.sortByDateOfCreation());
        model.addAttribute("sortedByDownloads", addonsService.sortByNumberOfDownloads());
        return "index";
    }

    @GetMapping("/about-us")
    public String showAboutUs() {
        return "about-us";
    }

}



