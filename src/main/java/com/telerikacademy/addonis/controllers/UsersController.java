package com.telerikacademy.addonis.controllers;

import com.telerikacademy.addonis.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import java.security.Principal;

/**
 * MVC controller with operations on Users - show users profile, show all users, show admin, enable/disable user.
 */

@Controller
public class UsersController {

    private UsersService usersService;


    @Autowired
    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping("/profile")
    public String getUser(Model model, Principal principal) {
        model.addAttribute("user", usersService.getUserByName(principal.getName()));
        return "user";
    }

    @GetMapping("/users")
    public String getAll(Model model) {
        model.addAttribute("all", usersService.getAll());
        return "admin-users";
    }

    @PostMapping("/{username}/disable")
    public String disableUser(@PathVariable String username) {
        usersService.disableUser(username);
        return "redirect:/users";
    }

    @PostMapping("/{username}/enable")
    public String enableUser(@PathVariable String username) {
        usersService.enableUser(username);
        return "redirect:/users";
    }
}

