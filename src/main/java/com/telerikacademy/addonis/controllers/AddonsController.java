package com.telerikacademy.addonis.controllers;

import com.telerikacademy.addonis.exceptions.DuplicateEntityException;
import com.telerikacademy.addonis.exceptions.EntityNotFoundException;
import com.telerikacademy.addonis.exceptions.InvalidDataException;
import com.telerikacademy.addonis.exceptions.InvalidOperationException;
import com.telerikacademy.addonis.models.*;
import com.telerikacademy.addonis.services.AddonOperationsHelper;
import com.telerikacademy.addonis.services.AddonsService;
import com.telerikacademy.addonis.services.RatingsService;
import com.telerikacademy.addonis.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Set;

/**
 * MVC controller with operations on Addons - show, create, update, delete, add and remove tags, rate.
 */

@Controller
@RequestMapping("/addons")
public class AddonsController {
    private UsersService usersService;
    private AddonsService addonsService;
    private RatingsService ratingsService;
    private Mapper mapper;

    @Autowired
    public AddonsController(UsersService usersService, AddonsService addonsService, RatingsService ratingsService, Mapper mapper) {
        this.usersService = usersService;
        this.addonsService = addonsService;
        this.ratingsService = ratingsService;
        this.mapper = mapper;
    }

    @GetMapping
    public String getAll(Model model) {
        model.addAttribute("addons", addonsService.getAllAddons());
        return "addons";
    }

    @GetMapping("/my-addons")
    public String getAll(Model model, HttpServletRequest request) {
        model.addAttribute("myaddons", addonsService.getAllUserAddons(request.getUserPrincipal().getName()));
        return "my-addons";
    }

    @GetMapping("/pending")
    public String getAllPendingAddons(Model model) {
        model.addAttribute("pendingaddons", addonsService.getAllPendingAddons());
        return "pending";
    }


    @GetMapping("/new")
    public String createAddonView(Model model, Principal principal) {
        model.addAttribute("addon", new AddonDto());
        return "create-addon";
    }

    @PostMapping("/new")
    public String createAddon(@Valid @ModelAttribute("addon") AddonDto addonDto,
                              @RequestParam("imageFile") MultipartFile imageFile,
                              @RequestParam("pluginFile") MultipartFile pluginFile,
                              BindingResult errors,
                              Model model,
                              Principal principal) {
        if (errors.hasErrors()) {
            model.addAttribute("error", "The required fields can not be empty!");
            return "create-addon";
        }
        try {
            Addon newAddon = new Addon();
            mapper.mapDtoToAddon(addonDto, newAddon);
            mapper.mapFilesDtoToAddon(newAddon, imageFile, pluginFile);
            addonsService.createAddon(newAddon, principal);
        } catch (DuplicateEntityException | InvalidDataException e) {
            model.addAttribute("error", e.getMessage());
            return "create-addon";
        }
        return "redirect:/addons/my-addons";
    }

    @GetMapping("/{id}")
    public String showAddonForm(@PathVariable String id, Model model, HttpServletRequest request) {
        try {
            Addon addon = addonsService.findById(id);
            AddonDto addonDto = new AddonDto();
            Detail details = addon.getDetail();
            Set<Tag> tags = addon.getTags();
            addonDto.setExisting(true);
            addonDto.setHasRightsToModify(AddonOperationsHelper.userHasRightsToModify(request, addon, usersService));
            mapper.mapDtoToShow(addonDto, addon);
            RatingDTO ratingDTO = new RatingDTO();
            ratingDTO.setAverageRating(ratingsService.getAverageAddonRating(id));
            model.addAttribute("addon", addonDto);
            model.addAttribute("addondetails", details);
            model.addAttribute("addontags", tags);
            model.addAttribute("ratingDTO", ratingDTO);
            return "update-addon";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/addons";
        }
    }

    @PostMapping("/{id}/update")
    public String update(@PathVariable String id,
                         @Valid @ModelAttribute AddonDto addonDto,
                         @RequestParam("imageFile") MultipartFile imageFile,
                         @RequestParam("pluginFile") MultipartFile pluginFile,
                         BindingResult errors,
                         Model model,
                         HttpServletRequest request) {
        if (errors.hasErrors()) {
            model.addAttribute("error", "The required fields can not be empty!");
            return "update-addon";
        }
        try {
            Addon addon = addonsService.findById(id);
            mapper.mapDtoToAddon(addonDto, addon);
            mapper.mapFilesDtoToAddon(addon, imageFile, pluginFile);
//todo add user role in User model and add isAdmin in the getter
            Principal principal = request.getUserPrincipal();
            User currentUser = usersService.getUserByName(principal.getName());
            boolean isAdmin = request.isUserInRole("ROLE_ADMIN");

            addonsService.update(addon, currentUser, isAdmin);
        } catch (EntityNotFoundException | InvalidOperationException | DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "update-addon";
        }
        return "redirect:/addons";
    }

    @PostMapping("/{id}/delete")
    public String delete(@PathVariable String id, Model model, HttpServletRequest request) {
        try {
            Principal principal = request.getUserPrincipal();
            User currentUser = usersService.getUserByName(principal.getName());
            boolean isAdmin = request.isUserInRole("ROLE_ADMIN");
            addonsService.delete(id, currentUser, isAdmin);
            return "redirect:/addons";
        } catch (EntityNotFoundException | InvalidOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "update-addon";
        }
    }

    @PostMapping("/{addonId}/add-tag")
    public String addTag(@PathVariable String addonId, @Valid @ModelAttribute TagDto tagDto, Model model, BindingResult errors) {
        if (errors.hasErrors()) {
            model.addAttribute("error", "Tag already added to this addon.");
            return "redirect:/addons/" + addonId;
        }
        Tag newTag = new Tag();
        mapper.mapDtoToTag(tagDto, newTag);
        addonsService.addTag(addonId, newTag);
        return "redirect:/addons/" + addonId;
    }

    @PostMapping("/{addonId}/remove-tag")
    public String removeTag(@PathVariable String addonId, @Valid @ModelAttribute TagDto tagDto) {
        addonsService.removeTag(addonId, tagDto.getTagName());
        return "redirect:/addons/" + addonId;
    }

    @GetMapping("/tag")
    public String filterByTag(Model model, @RequestParam int tagId) {
        model.addAttribute("addons", addonsService.filterByTag(tagId));
        return "addons";
    }

    @PostMapping("/{addonId}/rate")
    public String rateAddon(@ModelAttribute RatingDTO ratingDTO,
                            @PathVariable String addonId,
                            Principal principal) {

        Rating rating = mapper.mapRatingDtoToRating(addonId, principal.getName(), ratingDTO);
        ratingsService.addRatingToAddon(rating);

        return "redirect:/addons/" + addonId;
    }

    @PostMapping("/{addonId}/enable")
    public String enableUser(@PathVariable String addonId) {
        addonsService.enableAddon(addonId);
        return "redirect:/addons";
    }

    @GetMapping("/sort")
    public String sort(Model model, @RequestParam String criteria) {
        model.addAttribute("addons", addonsService.sort(criteria));
        return "addons";
    }

}
