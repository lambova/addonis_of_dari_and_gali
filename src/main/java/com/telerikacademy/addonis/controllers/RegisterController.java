package com.telerikacademy.addonis.controllers;

import com.telerikacademy.addonis.exceptions.DuplicateEntityException;
import com.telerikacademy.addonis.exceptions.EntityNotFoundException;
import com.telerikacademy.addonis.registration.OnPasswordChangeCompleteEvent;
import com.telerikacademy.addonis.registration.OnRegistrationCompleteEvent;
import com.telerikacademy.addonis.models.User;
import com.telerikacademy.addonis.models.VerificationToken;
import com.telerikacademy.addonis.services.UsersSecurityService;
import com.telerikacademy.addonis.services.UsersService;
import com.telerikacademy.addonis.models.PasswordDTO;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Calendar;
import java.util.Locale;

/**
 * MVC controller for registering new User.
 */

@Controller
public class RegisterController {

    private UsersService usersService;
    private ApplicationEventPublisher eventPublisher;
    private MessageSource messageSource;
    private UsersSecurityService usersSecurityService;

    @Autowired
    public RegisterController(UsersService usersService, ApplicationEventPublisher eventPublisher, MessageSource messageSource, UsersSecurityService usersSecurityService) {
        this.usersService = usersService;
        this.eventPublisher = eventPublisher;
        this.messageSource = messageSource;
        this.usersSecurityService = usersSecurityService;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new User());
        return "register-user";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute("user") User user, HttpServletRequest request, BindingResult errors, Model model) {
        if (errors.hasErrors()) {
            model.addAttribute("error", "Username/password/email can not be empty!");
            return "register-user";
        }

        if (!user.getPassword().equals(user.getPasswordConfirmation())) {
            model.addAttribute("error", "Password does not match!");
            return "register-user";
        }

        try {
            User registered = usersService.createUser(user);
            String appUrl = request.getContextPath();
            eventPublisher
                    .publishEvent(new OnRegistrationCompleteEvent
                            (registered, request.getLocale(), appUrl));
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "register-user";
        }

        return "confirm-registration";
    }

    @GetMapping("/confirm-registration")
    public String confirmRegistration
            (WebRequest request, Model model, @RequestParam("token") String token) {

        Locale locale = request.getLocale();

        VerificationToken verificationToken = usersService.getVerificationToken(token);

        User user = verificationToken.getUser();
        Calendar cal = Calendar.getInstance();
        if ((verificationToken.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            String messageValue = messageSource.getMessage("auth.message.expired", null, locale);
            model.addAttribute("message", messageValue);

            return "expired-authorisation";
        }

        usersService.enableUser(user.getUsername());
        return "login";
    }

    @GetMapping("/forgot-password")
    public String showForgotPassword() {
        return "forgot-password";
    }

    @PostMapping("/reset-password")
    public String resetPassword(HttpServletRequest request,
                                @RequestParam("email") String email, Model model) {
        try {
            User user = usersService.findUserByEmail(email);
            String appUrl = request.getContextPath();
            eventPublisher
                    .publishEvent(new OnPasswordChangeCompleteEvent
                            (user, request.getLocale(), appUrl));
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "forgot-password";
        }

        return "login";
    }

    @GetMapping("/change-password")
    public String showChangePasswordPage(Locale locale, Model model,
                                         @RequestParam("token") String token) {
        String result = usersSecurityService.validatePasswordResetToken(token);
        if (result != null) {
            model.addAttribute("error", "Reset token is expired.");
            return "update-password";
        }
        model.addAttribute("token", token);
        return "update-password";
    }

    @PostMapping("/save-password")
    public String savePassword(@Valid PasswordDTO passwordDTO, BindingResult errors, Model model) {
        model.addAttribute("passwordDTO", passwordDTO);
        if (errors.hasErrors()) {
            model.addAttribute("error", "Password/token can not be empty!");
            return "update-password";
        }
        String result = usersSecurityService.validatePasswordResetToken(passwordDTO.getToken());
        if (result != null) {
            model.addAttribute("error", "Reset token is invalid.");
            return "update-password";
        }
        User user = usersService.
                getUserByPasswordResetToken(passwordDTO.getToken());
        usersService.changeUserPassword(user, passwordDTO.getNewPassword());
        return "login";
    }
}

