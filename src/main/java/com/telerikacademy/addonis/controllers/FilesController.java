package com.telerikacademy.addonis.controllers;

import com.telerikacademy.addonis.exceptions.EntityNotFoundException;
import com.telerikacademy.addonis.exceptions.InvalidOperationException;
import com.telerikacademy.addonis.models.Addon;
import com.telerikacademy.addonis.services.AddonOperationsHelper;
import com.telerikacademy.addonis.services.AddonsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletResponse;

/**
 * MVC controller for downloading files - image and plugin.
 */

@Controller
public class FilesController {

    private AddonsService addonsService;

    @Autowired
    public FilesController(AddonsService addonsService) {
        this.addonsService = addonsService;
    }

    @GetMapping("/{id}/addonImage")
    public void takeAddonImageFromDB(@PathVariable String id, Model model, HttpServletResponse response) {
        try {
            Addon addon = addonsService.findById(id);
            AddonOperationsHelper.getByteArray(addon.getImage(), response);
        } catch (EntityNotFoundException | InvalidOperationException e) {
            model.addAttribute("error", e.getMessage());
        }
    }

    @GetMapping("/{id}/download")
    public void takeAddonPluginFromDB(@PathVariable String id, Model model, HttpServletResponse response) {
        try {
            Addon addon = addonsService.findById(id);
            AddonOperationsHelper.getByteArray(addon.getPlugin(), response);
            addon.setNumberOfDownloads(addon.getNumberOfDownloads()+1);
            addonsService.update(addon);
        } catch (EntityNotFoundException | InvalidOperationException e) {
            model.addAttribute("error", e.getMessage());
        }
    }


}
