package com.telerikacademy.addonis.controllers.rest;

import com.telerikacademy.addonis.models.User;
import com.telerikacademy.addonis.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
/**
 * Rest controller for CRUD operations on Users.
 */

@RestController
@RequestMapping("/api/users")
public class UsersRestController {
    private UsersService usersService;

    @Autowired
    public UsersRestController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping
    public Collection<User> getUser() {
        return usersService.getAll();
    }

    @GetMapping("/{username}")
    public User getByUsername(@PathVariable String username) {
        return usersService.getUserByName(username);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/{username}/disable")
    public User disableUser(@PathVariable String username) {
        User user = getByUsername(username);
        usersService.disableUser(username);
        return user;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping("/{username}/enable")
    public User enableUser(@PathVariable String username) {
        User user = getByUsername(username);
        usersService.enableUser(username);
        return user;
    }


}
