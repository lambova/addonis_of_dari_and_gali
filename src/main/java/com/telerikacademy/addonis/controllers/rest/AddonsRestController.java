package com.telerikacademy.addonis.controllers.rest;

import com.telerikacademy.addonis.exceptions.EntityNotFoundException;
import com.telerikacademy.addonis.models.Addon;
import com.telerikacademy.addonis.models.AddonDto;
import com.telerikacademy.addonis.models.Mapper;
import com.telerikacademy.addonis.models.User;
import com.telerikacademy.addonis.services.AddonsService;
import com.telerikacademy.addonis.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.security.Principal;
import java.util.List;

/**
 * Rest controller for CRUD operations on Addons.
 */

@RestController
@RequestMapping("/api/addons")
public class AddonsRestController {
    private AddonsService addonsService;
    private UsersService usersService;
    private Mapper mapper;

    @Autowired
    public AddonsRestController(AddonsService addonsService, UsersService usersService, Mapper mapper) {
        this.addonsService = addonsService;
        this.usersService = usersService;
        this.mapper = mapper;
    }

    @GetMapping
    public List<Addon> getAll(@RequestParam(required = false) String name) {
        return addonsService.getAllAddons();
    }

    @GetMapping("/{id}")
    public Addon getById(@PathVariable String id) {
        try {
            return addonsService.findById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Addon create(@RequestBody @Valid AddonDto addonDto, Principal principal) {
        Addon newAddon = new Addon();
        mapper.mapDtoToAddon(addonDto, newAddon);
        return addonsService.createAddon(newAddon, principal);
    }

    @PutMapping("/{id}")
    public Addon update(@PathVariable String id,
                        @RequestBody @Valid AddonDto addonDto,
                        HttpServletRequest request) {
        Addon addon = addonsService.findById(id);
        mapper.mapDtoToAddon(addonDto, addon);

        Principal principal = request.getUserPrincipal();
        User currentUser = usersService.getUserByName(principal.getName());
        boolean isAdmin = request.isUserInRole("ROLE_ADMIN");

        addonsService.update(addon, currentUser, isAdmin);
        return addon;
      }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable String id, HttpServletRequest request) {
        Principal principal = request.getUserPrincipal();
        User currentUser = usersService.getUserByName(principal.getName());
        boolean isAdmin = request.isUserInRole("ROLE_ADMIN");
        addonsService.delete(id, currentUser, isAdmin);
    }

}
