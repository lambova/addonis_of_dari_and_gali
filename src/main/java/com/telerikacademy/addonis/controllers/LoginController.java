package com.telerikacademy.addonis.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * MVC controller for login.
 */

@Controller
public class LoginController {

    @GetMapping("/login")
    public String showLogin(){
        return "login";
    }

}
