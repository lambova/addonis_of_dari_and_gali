package com.telerikacademy.addonis;

import com.telerikacademy.addonis.models.Addon;
import com.telerikacademy.addonis.models.Rating;
import com.telerikacademy.addonis.models.Tag;
import com.telerikacademy.addonis.models.User;


public class Factory {
    public static Addon createAddon() {
        return new Addon("1", "Add new file", "You can add file with this app.","https://github.com/VsVim/VsVim");
    }

    public static User createUser() {
        return new User();
    }

    public static Tag createTag() {
        return new Tag();
    }

    public static Rating createRating() {
        return new Rating();
    }

}
