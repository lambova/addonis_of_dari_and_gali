package com.telerikacademy.addonis.services;

import com.telerikacademy.addonis.models.Addon;
import com.telerikacademy.addonis.models.Rating;
import com.telerikacademy.addonis.models.User;
import com.telerikacademy.addonis.repositories.RatingsRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.telerikacademy.addonis.Factory.*;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class RatingServiceImplTests {

    @Mock
    RatingsRepository mockRepository;

    @InjectMocks
    RatingsServiceImpl mockService;

    @Test
    public void addRatingToAddon_Should_Add_Rating_To_Addon() {
        //Arrange
        Addon addon = createAddon();

        User user = createUser();
        user.setUsername("galina");

        Rating rating = createRating();
        rating.setAddon(addon);
        rating.setUser(user);

        Rating newRating = createRating();
        newRating.setAddon(addon);
        newRating.setUser(user);

        Mockito.when(mockRepository.existsByUser_UsernameAndAddon_Id(user.getUsername(),addon.getId()))
                .thenReturn(true);
        Mockito.when(mockRepository.findByUser_UsernameAndAddon_Id(user.getUsername(),addon.getId()))
                .thenReturn(rating);

        //Act
        mockService.addRatingToAddon(newRating);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).save(rating);
    }

    @Test
    public void getAverageAddonRating() {
        //Arrange, Act
        mockService.getAverageAddonRating(anyString());

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).findAverageAddonRating(anyString());
    }
}
