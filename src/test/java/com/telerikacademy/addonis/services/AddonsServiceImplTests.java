package com.telerikacademy.addonis.services;

import com.telerikacademy.addonis.exceptions.DuplicateEntityException;
import com.telerikacademy.addonis.exceptions.EntityNotFoundException;
import com.telerikacademy.addonis.exceptions.InvalidOperationException;
import com.telerikacademy.addonis.models.Addon;
import com.telerikacademy.addonis.models.Detail;
import com.telerikacademy.addonis.models.Tag;
import com.telerikacademy.addonis.models.User;
import com.telerikacademy.addonis.repositories.AddonsRepository;
import com.telerikacademy.addonis.repositories.GitHubDetailsRepository;
import com.telerikacademy.addonis.repositories.TagsRepository;
import io.swagger.models.Response;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.security.Principal;
import java.util.*;

import static com.telerikacademy.addonis.Factory.*;
import static org.mockito.ArgumentMatchers.*;

@RunWith(MockitoJUnitRunner.class)
public class AddonsServiceImplTests {
    @Mock
    AddonsRepository mockRepository;

    @Mock
    GitHubDetailsRepository mockGithubRepository;

    @Mock
    UsersServiceImpl mockUserService;

    @Mock
    TagsRepository mockTagsRepository;

    @Mock
    Principal mockPrincipal;

    @InjectMocks
    AddonsServiceImpl mockService;


    @Test
    public void getById_should_returnAddon_whenAddonExists() {
        //Arrange
        Addon expectedAddon = createAddon();

        Mockito.when(mockRepository.findById(anyString()))
                .thenReturn(java.util.Optional.of(expectedAddon));

        //Act
        Addon returnedAddon = mockService.findById(anyString());

        //Assert
        Assert.assertSame(expectedAddon, returnedAddon);
    }

    @Test
    public void getAllAddons_Should_Return_EmptyList() {
        //Arrange
        Mockito.when(mockRepository.getAllAddons())
                .thenReturn(new ArrayList<>());
        //Act
        mockService.getAllAddons();
        //Assert
        Assert.assertTrue(mockRepository.getAllAddons().isEmpty());
    }

    @Test
    public void getAllPendingAddons_Should_Return_EmptyList() {
        //Arrange
        Mockito.when(mockRepository.getAllPendingAddons())
                .thenReturn(new ArrayList<>());
        //Act
        mockService.getAllPendingAddons();
        //Assert
        Assert.assertTrue(mockRepository.getAllPendingAddons().isEmpty());
    }

    @Test
    public void getAllUserAddons_Should_Return_EmptyList()  {
        //Arrange
        Mockito.when(mockRepository.getAllUserAddons(anyString()))
                .thenReturn(new ArrayList<>());
        //Act
        mockService.getAllUserAddons(anyString());
        //Assert
        Assert.assertTrue(mockRepository.getAllUserAddons(anyString()).isEmpty());
    }

    @Test
    public void findByName_should_returnAddon_whenAddonExists() {
        //Arrange
        Addon expectedAddon = createAddon();
        Mockito.when(mockRepository.findByName(anyString()))
                .thenReturn(expectedAddon);

        //Act
        Addon returnedAddon = mockService.findByName(anyString());

        //Assert
        Assert.assertSame(expectedAddon, returnedAddon);
    }

    @Test(expected = EntityNotFoundException.class)
    public void findByName_Throws_whenAddonDoesNotExists() {
        //Act
        mockService.findByName(anyString());
    }

    @Test
    public void getAllAddons_Should_Return_AllAddons() {
        //Arrange
        Mockito.when(mockRepository.getAllAddons())
                .thenReturn(Arrays.asList(
                        createAddon(),
                        createAddon()
                ));

        //Act
        mockService.getAllAddons();

        //Assert
        Assert.assertEquals(2, mockRepository.getAllAddons().size());
    }

    @Test
    public void createAddon_should_addInRepository() {
        //Arrange
        Addon addon = createAddon();
        Mockito.when(mockPrincipal.getName()).thenReturn("user");
        Mockito.when(mockGithubRepository.createAddonDetails(anyString())).thenReturn(new Detail());
        Mockito.when(mockUserService.getUserByName(anyString())).thenReturn(any(User.class));

        //Act
        mockService.createAddon(addon, mockPrincipal);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).save(addon);
    }

    @Test(expected = InvalidOperationException.class)
    public void updateAddon_Throws_When_User_Cannot_Update_Addon() {
        //Arrange
        Addon addon = createAddon();
        User user1 = createUser();
        user1.setUsername("darina");
        addon.setCreator(user1);
        User user = createUser();
        user.setUsername("galina");
        boolean isAdmin = false;

        //Act
        mockService.update(addon, user, isAdmin);
    }

    @Test
    public void updateAddon_Should_Update_Addon() {
        //Arrange
        Addon addon = createAddon();
        User user = createUser();
        addon.setCreator(user);
        user.setUsername("galina");
        boolean isAdmin = false;

        //Act
        mockService.update(addon, user, isAdmin);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).save(addon);
    }

    @Test
    public void update_Should_Update_Addon() {
        //Arrange
        Addon addon = createAddon();

        //Act
        mockService.update(addon);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).save(addon);
    }

    @Test(expected = InvalidOperationException.class)
    public void deleteAddon_Throws_When_User_Cannot_Delete_Addon() {
        //Arrange
        Addon addon = createAddon();
        User user1 = createUser();
        user1.setUsername("darina");
        addon.setCreator(user1);
        User user = createUser();
        user.setUsername("galina");
        boolean isAdmin = false;
        Mockito.when(mockRepository.findById(anyString())).thenReturn(Optional.of(addon));

        //Act
        mockService.delete(addon.getId(), user, isAdmin);
    }

    @Test
    public void deleteAddon_Should_Delete_Addon() {
        //Arrange
        Addon addon = createAddon();
        User user1 = createUser();
        user1.setUsername("darina");
        addon.setCreator(user1);
        User user = createUser();
        user.setUsername("galina");
        boolean isAdmin = true;
        Mockito.when(mockRepository.findById(anyString())).thenReturn(Optional.of(addon));

        //Act
        mockService.delete(addon.getId(), user, isAdmin);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).save(addon);
    }

    @Test
    public void addTag_Should_Add_Tag_To_Addon() {
        //Arrange
        Tag tag = createTag();
        tag.setTagName("Intelij");
        List<Tag> tags = new ArrayList<>();
        Mockito.when(mockTagsRepository.findAll()).thenReturn(tags);
        Mockito.when(mockTagsRepository.findByTagName(anyString())).thenReturn(tag);

        Addon addon = createAddon();
        Mockito.when(mockRepository.findById(anyString())).thenReturn(Optional.of(addon));

        //Act
        mockService.addTag(addon.getId(), tag);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).save(addon);
    }

    @Test
    public void removeTag_Should_Remove_Tag_To_Addon() {
        //Arrange
        Addon addon = createAddon();
        Mockito.when(mockRepository.findById(anyString())).thenReturn(Optional.of(addon));
        Tag tag = createTag();
        tag.setTagName("Intelij");
        Mockito.when(mockTagsRepository.findByTagName(anyString())).thenReturn(tag);

        //Act
        mockService.removeTag(addon.getId(), tag.getTagName());

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).save(addon);
    }

    @Test
    public void filterByTag_Should_Return_EmptyList() {
        //Arrange, Act
        mockService.filterByTag(anyInt());
        //Assert
        Assert.assertTrue(mockTagsRepository.filterByTag(anyInt()).isEmpty());
    }

    @Test
    public void getAllTags_Should_Return_EmptyList() {
        //Arrange
        Mockito.when(mockTagsRepository.findAll())
                .thenReturn(new ArrayList<>());
        //Act
        mockService.getAllTags();
        //Assert
        Assert.assertTrue(mockTagsRepository.findAll().isEmpty());
    }

    @Test
    public void enableAddon_Should_Enable_Addon (){
        //Arrange
        Addon addon = createAddon();
        Mockito.when(mockRepository.findById(anyString())).thenReturn(Optional.of(addon));

        //Act
        mockService.enableAddon(addon.getName());

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).save(addon);
    }

    @Test
    public void sortByName_Should_Return_From_Repository() {
        //Arrange, Act
        mockService.sortByName();
        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).findAllOrderByName();
    }


    @Test
    public void sortByNameDesc_Should_Return_From_Repository() {
        //Arrange, Act
        mockService.sortByNameDesc();
        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).findAllOrderByNameDesc();
    }

    @Test
    public void sort_Should_Return_From_RepositoryByName() {
        //Arrange, Act
        mockService.sort("name");
        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).findAllOrderByName();
    }

    @Test
    public void sort_Should_Return_From_RepositoryByNameDesc() {
        //Arrange, Act
        mockService.sort("name_desc");
        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).findAllOrderByNameDesc();
    }

    @Test
    public void sort_Should_Return_From_RepositoryByNumberOfDownloads() {
        //Arrange, Act
        mockService.sort("downloads");
        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).findAllOrderByNumberOfDownloads();
    }

    @Test
    public void sort_Should_Return_From_RepositoryByDateOfCreation() {
        //Arrange, Act
        mockService.sort("upload_date");
        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).findAllOrderByDateOfCreation();
    }

    @Test
    public void sort_Should_Return_From_RepositoryByDateOfLastCommit() {
        //Arrange, Act
        mockService.sort("commit_date");
        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).findAllOrderByDetailDateOfLastCommit();
    }

    @Test
    public void sort_Should_Return_From_RepositoryGetAll() {
        //Arrange,
        Mockito.when(mockRepository.getAllAddons())
                .thenReturn(Arrays.asList(
                        createAddon(),
                        createAddon()
                ));

        // Act
        mockService.sort("0");

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAllAddons();
    }

}

