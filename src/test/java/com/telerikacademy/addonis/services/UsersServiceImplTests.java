package com.telerikacademy.addonis.services;

import com.telerikacademy.addonis.exceptions.DuplicateEntityException;
import com.telerikacademy.addonis.exceptions.EntityNotFoundException;
import com.telerikacademy.addonis.models.User;
import com.telerikacademy.addonis.repositories.UsersRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import javax.swing.text.html.parser.Entity;
import java.util.ArrayList;

import static com.telerikacademy.addonis.Factory.createUser;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class UsersServiceImplTests {

    @Mock
    UsersRepository mockRepository;

    @Mock
    UserDetailsManager mockUserDetailsManager;

    @Mock
    PasswordEncoder mockPasswordEncoder;

    @InjectMocks
    UsersServiceImpl mockService;

    @Test
    public void getAll_Should_Return_EmptyList() {
        //Arrange
        Mockito.when(mockRepository.findAll())
                .thenReturn(new ArrayList<>());

        //Act
        mockService.getAll();

        //Assert
        Assert.assertTrue(mockRepository.findAll().isEmpty());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getUserByName_Should_Throw_When_User_Doesnot_Exist() {
        //Arrange
       Mockito.when(mockUserDetailsManager.userExists(anyString())).thenReturn(false);

       //Act
       mockService.getUserByName(anyString());
    }

    @Test
    public void getUserByName_Should_Return_User_By_Username() {
        //Arrange
        Mockito.when(mockUserDetailsManager.userExists(anyString())).thenReturn(true);

       //Act
        mockService.getUserByName(anyString());

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).findByUsername(anyString());
    }

    @Test (expected = DuplicateEntityException.class)
    public void createUser_Should_Throw_When_User_Exists (){
        //Arrange
        Mockito.when(mockUserDetailsManager.userExists(anyString())).thenReturn(true);
        User user = createUser();
        user.setUsername("galina");

        //Act
        mockService.createUser(user);
    }

    @Test
    public void createUser_Should_Create_User (){
        //Arrange
        Mockito.when(mockUserDetailsManager.userExists(anyString())).thenReturn(false);
        User user = createUser();
        user.setUsername("galina");
        user.setPassword("12345");
        Mockito.when(mockPasswordEncoder.encode("12345")).thenReturn("54321");

        //Act
        mockService.createUser(user);

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).save(user);

    }

    @Test
    public void disableUser_Should_Disable_User (){
        //Arrange
        User user = createUser();
        user.setUsername("galina");
        Mockito.when(mockUserDetailsManager.userExists(anyString())).thenReturn(true);
        Mockito.when(mockRepository.findByUsername(user.getUsername())).thenReturn(user);

        //Act
        mockService.disableUser(user.getUsername());

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).save(user);
    }

    @Test
    public void enableUser_Should_Enable_User (){
        //Arrange
        User user = createUser();
        user.setUsername("galina");
        Mockito.when(mockUserDetailsManager.userExists(anyString())).thenReturn(true);
        Mockito.when(mockRepository.findByUsername(user.getUsername())).thenReturn(user);

        //Act
        mockService.enableUser(user.getUsername());

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).save(user);
    }
}
