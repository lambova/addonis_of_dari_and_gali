#ReadMe
#Addonis

Addonis is an Addons Registry web application. Some of the actions it enables its users to do are:
- Publish their own addons;
- Download addons;
- Rate existing addons.

In order to set up the application, 
- First run the dependencies and build.gradle;
- Then run the database script and dump data;
- Finally start AddonisApplication.

This is our API Documentation: http://localhost:8080/swagger-ui.html

This is our trello board: https://trello.com/b/tXKEmxwA/darigalia

Created by Galina Lambova and Darina Ilieva.