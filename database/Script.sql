-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.12-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for addonis
DROP DATABASE IF EXISTS `addonis`;
CREATE DATABASE IF NOT EXISTS `addonis` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `addonis`;

-- Dumping structure for table addonis.addons
DROP TABLE IF EXISTS `addons`;
CREATE TABLE IF NOT EXISTS `addons` (
  `addon_id` varchar(50) NOT NULL,
  `addon_name` varchar(50) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  `number_of_downloads` int(11) DEFAULT NULL,
  `link_origin` varchar(100) DEFAULT NULL,
  `binary_content` longblob DEFAULT NULL,
  `creator_username` varchar(50) NOT NULL,
  `details_id` int(11) DEFAULT NULL,
  `image` blob DEFAULT NULL,
  `is_enabled` tinyint(4) DEFAULT 0,
  `is_deleted` tinyint(4) DEFAULT NULL,
  `date_of_creation` mediumtext DEFAULT NULL,
  PRIMARY KEY (`addon_id`),
  KEY `addons_details_details_id_fk` (`details_id`),
  KEY `addons_users_username_fk` (`creator_username`),
  CONSTRAINT `addons_details_details_id_fk` FOREIGN KEY (`details_id`) REFERENCES `details` (`details_id`),
  CONSTRAINT `addons_users_username_fk` FOREIGN KEY (`creator_username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table addonis.addons_tags
DROP TABLE IF EXISTS `addons_tags`;
CREATE TABLE IF NOT EXISTS `addons_tags` (
  `addon_id` varchar(50) DEFAULT NULL,
  `tag_id` int(11) DEFAULT NULL,
  KEY `addons_tags_addons_addon_id_fk` (`addon_id`),
  KEY `addons_tags_tags_tag_id_fk` (`tag_id`),
  CONSTRAINT `addons_tags_addons_addon_id_fk` FOREIGN KEY (`addon_id`) REFERENCES `addons` (`addon_id`),
  CONSTRAINT `addons_tags_tags_tag_id_fk` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table addonis.authorities
DROP TABLE IF EXISTS `authorities`;
CREATE TABLE IF NOT EXISTS `authorities` (
  `username` varchar(50) NOT NULL,
  `authority` varchar(10) DEFAULT NULL,
  UNIQUE KEY `username_authorities` (`username`,`authority`),
  CONSTRAINT `authorities_users_username_fk` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table addonis.details
DROP TABLE IF EXISTS `details`;
CREATE TABLE IF NOT EXISTS `details` (
  `details_id` int(11) NOT NULL AUTO_INCREMENT,
  `count_open_issues` int(11) DEFAULT 0,
  `count_pull_requests` int(11) DEFAULT 0,
  `date_last_commit` varchar(50) DEFAULT 'No date',
  `message_last_commit` varchar(400) DEFAULT 'no commit',
  PRIMARY KEY (`details_id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table addonis.ratings
DROP TABLE IF EXISTS `ratings`;
CREATE TABLE IF NOT EXISTS `ratings` (
  `rating_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_username` varchar(50) DEFAULT NULL,
  `addon_id` varchar(50) DEFAULT NULL,
  `rating_points` int(11) DEFAULT NULL,
  PRIMARY KEY (`rating_id`),
  KEY `ratings_users_username_fk` (`user_username`),
  KEY `ratings_addons_addon_id_fk` (`addon_id`),
  CONSTRAINT `ratings_addons_addon_id_fk` FOREIGN KEY (`addon_id`) REFERENCES `addons` (`addon_id`),
  CONSTRAINT `ratings_users_username_fk` FOREIGN KEY (`user_username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table addonis.tags
DROP TABLE IF EXISTS `tags`;
CREATE TABLE IF NOT EXISTS `tags` (
  `tag_id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table addonis.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(68) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `enabled` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `users_username_uindex` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

-- Dumping structure for table addonis.verification_tokens
DROP TABLE IF EXISTS `verification_tokens`;
CREATE TABLE IF NOT EXISTS `verification_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(300) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `verification_tokens_users_username_fk` (`username`),
  CONSTRAINT `verification_tokens_users_username_fk` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
